import { ClientOptions } from "discordx";
import { AppOptions } from "../intefaces/appoptions.interface";

const notClientOptionsKey = ["dbClasses"];

export function mapAppToClientOptions(options: AppOptions): ClientOptions {
  return omit(options, notClientOptionsKey);
}

export const omit = (obj: Object, keys: string[]) => {
  const output = [];
  for (const [key, value] of Object.entries(obj)) {
    if (!keys.includes(key)) {
      output.push([key, value]);
    }
  }
  return Object.fromEntries(output);
};
