import { Client, ClientOptions, DIService } from "discordx";
import { Logger } from "tslog";
import { container } from "tsyringe";
import { Connection, createConnection } from "typeorm";
import { registerDbSettings } from "./DI/registerDbSettings";
import { registerServices } from "./DI/registerServices";
import { AppOptions } from "./intefaces/appoptions.interface";
import { mapAppToClientOptions } from "./utils/mapAppToClientOptions";
import connectionOptions from "../database/ormconfig";
import { registerControllers } from "./DI/registerControllers";
import * as Express from "express";
import cors = require("cors");

export class App {
  public name!: string;
  private discord!: Client;
  private db!: Connection;
  private logger!: Logger;
  private options!: AppOptions;
  private api!: Express.Application;
  private router!: Express.Router;
  private server!: any;

  constructor(name: string, discord: Client, options: AppOptions) {
    this.name = name;
    this.discord = discord;
    this.logger = new Logger({ name });
    this.options = options;
    if (options.useExpress) {
      this.api = Express();
      this.initExpress();
    }
  }

  public static create(name: string, options: AppOptions): App {
    DIService.container = container;
    const clientOptions = mapAppToClientOptions(options);
    return new App(name, new Client(clientOptions), options);
  }

  private initExpress(): void {
    this.api.use(Express.json());
    this.api.use(cors());
    this.api.use(Express.urlencoded({ extended: true }));
  }

  public async start(): Promise<void> {
    try {
      await registerServices(this.discord, this.logger);
      if (this.options.useDatabase) {
        await this.startDB();
        await registerDbSettings(this.options.dbClasses);
      }
      await this.startDiscord();
    } catch (e) {
      console.log(e);
    }
  }

  public stop(): void {
    if (this.server) {
      this.server.close(() => {
        this.logger.info(`[Server] Api shutdown`);
        process.exit(0);
      });
    }
  }

  private async loadControllers(): Promise<void> {
    const controllers = await registerControllers(
      this.options.controllerClasses
    );
    if (controllers) {
      controllers.forEach(
        ({ route, router }: { route: string; router: Express.Router }) => {
          this.api.use(route, router);
          this.logger.info(`[Server] Registered router ${route}`);
        }
      );
    }
  }

  private startExpress(): void {
    const port = process.env.SERVER_PORT ?? "3000";
    this.server = this.api.listen(port, () => {
      this.logger.info(`[Server] Api is running at http://localhost:${port}`);
    });
  }

  private async startDiscord(): Promise<void> {
    const token = process.env.DISCORD_TOKEN ?? "";
    if (!token)
      throw new Error("Bot cant start please provide DISCORD_TOKEN in .env");
    this.listenReady();
    await this.discord.login(token);
  }

  private async startDB(): Promise<void> {
    this.db = process.env.DATABASE_URL
      ? await createConnection({
          ...connectionOptions,
          url: process.env.DATABASE_URL,
        } as any)
      : await createConnection({
          ...connectionOptions,
          host: process.env.DB_HOST,
          port: process.env.DB_PORT,
          username: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_DATABASE,
        } as any);
    this.logger.info(
      `[Server] Database running with ${connectionOptions.type} on ${connectionOptions.database}`
    );
    await registerServices(this.db);
  }

  private listenReady(): void {
    this.discord.on("ready", async () => {
      this.logger.info(`Discord READY`);
      await this.discord.initApplicationCommands();
      await this.discord.initApplicationPermissions();
      if (this.options.useExpress) {
        await this.loadControllers();
        this.startExpress();
      }
    });
  }
}
