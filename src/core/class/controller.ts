import { Settings } from "../../database/entity/Settings";
import { injectable } from "tsyringe";
import { Connection, Repository } from "typeorm";
import { IController } from "../intefaces/controller.interface";
import * as Express from "express";

@injectable()
export class Controller implements IController {
  route!: string;
  router: Express.Router = Express.Router();
}
