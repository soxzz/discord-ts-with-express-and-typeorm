import { Settings } from "../../database/entity/Settings";
import { injectable } from "tsyringe";
import { Connection, Repository } from "typeorm";
import { IGlobalSettings } from "../intefaces/globalsettings.interface";

@injectable()
export class GlobalSettings implements IGlobalSettings {
  id!: number;
  moduleEnabled = false;
  moduleName: string;
  moduleSettings: any = {};
  repository!: Repository<Settings>;
  private settings!: Partial<Settings>;

  constructor(private _db: Connection) {
    this.moduleName = (this as any).constructor.name;
  }

  async initSettings(): Promise<Partial<Settings> | void> {
    this.repository = this._db.getRepository("Settings");
    let setting: Partial<Settings> | undefined = await this.repository.findOne({
      moduleName: this.moduleName,
    });
    if (!setting) {
      setting = await this.repository.save({
        moduleName: this.moduleName,
        moduleSettings: this.moduleSettings,
      });
      this.moduleSettings = setting.moduleSettings;
    } else {
      this.moduleEnabled = setting.moduleEnabled ?? false;
      this.moduleSettings = setting.moduleSettings ?? {};
      if (setting.id) this.id = setting.id;
    }
    this.settings = setting;
    /*console.log(
      `Settings initialized for ${this.moduleName}`,
      this.moduleSettings
    );*/
    return setting;
  }

  getSettings<T>(key: T): any {
    return this.moduleSettings[key];
  }

  async getAllSettings(): Promise<Object | undefined> {
    return (
      await this.repository.findOne({
        moduleName: this.moduleName,
      })
    )?.moduleSettings;
  }

  async setSettings<T>(key: T, val: any): Promise<void> {
    this.moduleSettings[key] = val;
    this.settings.moduleSettings = this.moduleSettings;
    await this.repository.save(this.settings);
  }
}
