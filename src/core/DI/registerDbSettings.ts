import { container } from "tsyringe";
import * as glob from "glob";

export async function registerDbSettings(dbClasses: string[]): Promise<void> {
  if (dbClasses.length > 0) {
    await dbClasses.forEach(async (path: string) => {
      const files = glob
        .sync(path)
        .filter((file: any) => typeof file === "string");
      await files.forEach(async (file: string) => {
        const dbSettings: any = require(file);
        const settings: any = new (Object.values(dbSettings)[0] as any)();
        const service = container.resolve(settings.constructor);
        await (service as any).initSettings();
      });
    });
  }
}
