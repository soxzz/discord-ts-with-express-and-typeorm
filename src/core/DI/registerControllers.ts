import * as glob from "glob";
import { container } from "tsyringe";
import * as Express from "express";
import { Controller } from "../class/controller";

export async function registerControllers(
  controllerClasses: string[]
): Promise<Array<{ route: string; router: Express.Router }>> {
  const controllers: Array<{ route: string; router: Express.Router }> = [];
  if (controllerClasses.length > 0) {
    await controllerClasses.forEach(async (path: string) => {
      const files = glob
        .sync(path)
        .filter((file: any) => typeof file === "string");
      await files.forEach(async (file: string) => {
        const controllerSettings: any = require(file);
        const controller: any = new (Object.values(
          controllerSettings
        )[0] as any)();
        const service: Controller = container.resolve(controller.constructor);
        controllers.push({ route: service.route, router: service.router });
      });
    });
  }
  return controllers;
}
