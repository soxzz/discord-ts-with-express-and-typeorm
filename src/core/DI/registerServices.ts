import { container } from "tsyringe";

export function registerServices(...instances: any): void {
  for (const instance of instances) {
    container.registerInstance(instance.constructor, instance);
  }
}
