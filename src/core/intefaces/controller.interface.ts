import * as Express from "express";

export interface IController {
  route: string;
  router: Express.Router;
}
