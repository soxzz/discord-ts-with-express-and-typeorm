import { ClientOptions } from "discordx";

export interface AppOptions extends ClientOptions {
  dbClasses: string[];
  controllerClasses: string[];
  useExpress: boolean;
  useDatabase: boolean;
}
