import { Settings } from "../../database/entity/Settings";

export interface IGlobalSettings {
  moduleEnabled: boolean;
  moduleName: string;
  moduleSettings: any;
  initSettings: () => Promise<Partial<Settings> | void>;
  getSettings: (key: string) => any;
  setSettings(key: string, val: any): Promise<void>;
}
