import "reflect-metadata";
import * as dotenv from "dotenv";
import { Intents } from "discord.js";

import { App } from "./core/app";

dotenv.config();

const AppName = process.env.DISCORD_BOT_NAME ?? "";
const AppAllowedServer = process.env.DISCORD_ALLOWED_SERVER ?? "";
if (!AppAllowedServer)
  throw new Error("Please enter in .env DISCORD_ALLOWED_SERVER");

const app: App = App.create(AppName, {
  botId: `${AppName}Bot`,
  botGuilds: [AppAllowedServer],
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_PRESENCES,
    Intents.FLAGS.DIRECT_MESSAGES,
  ],
  classes: [`${__dirname}/commands/**/*.{ts,js}`],
  dbClasses: [`${__dirname}/services/**/*.settings.{ts,js}`],
  controllerClasses: [`${__dirname}/controllers/**/*.{ts,js}`],
  silent: false,
  useExpress: true,
  useDatabase: true,
});

(async (): Promise<void> => {
  await app.start();
})();

process.on("SIGTERM", () => {
  app.stop();
});
