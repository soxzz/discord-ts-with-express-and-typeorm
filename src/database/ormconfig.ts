import { ConnectionOptions } from "typeorm";

export default {
  type: "postgres",
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  dropSchema: false,
  synchronize: false,
  logging: false,
  migrationsRun: true,
  entities: ["dist/database/entity/**/*.js"],
  migrations: ["dist/database/migration/*.js"],
  cli: {
    entitiesDir: "src/database/entity",
    migrationsDir: "src/database/migration",
  },
} as ConnectionOptions;
