# Discord TS with express and Typeorm

Based on :

- [DiscordTS](https://github.com/oceanroleplay/discord.ts) / [Documentation](https://discord-ts.js.org/)
- [tsyringe](https://github.com/microsoft/tsyringe)
- [Express](https://expressjs.com/fr/)
- [TypeORM](https://expressjs.com/fr/)

## Get started

Install all dependencies with:

```bash
$ npm i
```

Copy **.env.example** to **.env** and fill it

## How to develop with

- Commands folder will have your discord command and event
- Controllers folder will have your express API routes
- Database basic typeorm configuration with entity and migrations
- Guard folder will have the guard from DiscordTS
- Services folder will have your service or if you choose to useDatabase:
  - service.key.ts (containing your enum of key)
  - service.settings.ts extends GlobalSetings it's use to implement your module settings in the Database in JSON
  - service.service.ts is the entry point of your discord interaction, or DB call.

## Examples

### captcha.keys.ts

```typescript
export enum CaptchaSettingsKey {
  INITIAL_ROLE_ID = "initialRoleId",
  GIVEN_ROLE_ID = "givenRoleId",
  CHANNEL_ID = "channelId",
  ENABLED = "enabled",
}
```

### captcha.settings.ts

```typescript
import { GlobalSettings } from "@/core/class/settings";
import { singleton } from "tsyringe";
import { CaptchaSettingsKey } from "./captcha.keys";

@singleton()
export class CaptchaSettings extends GlobalSettings {
  moduleSettings: { [key in CaptchaSettingsKey]: any } = {
    initialRoleId: "",
    givenRoleId: "",
    channelId: "",
    enabled: false,
  };
}
```

### captcha.service.ts

```typescript
import {
  ApplicationCommandOptionChoice,
  AutocompleteInteraction,
  CommandInteraction,
  GuildChannel,
  GuildMember,
  Message,
  MessageCollector,
  MessageEmbed,
  Role,
  TextChannel,
  ThreadChannel,
} from "discord.js";
import { Client } from "discordx";
import { compareTwoStrings } from "string-similarity";
import { singleton } from "tsyringe";
import { CaptchaSettingsKey } from "./captcha.keys";
import { CaptchaSettings } from "./captcha.settings";
import * as svgCaptcha from "svg-captcha";
import * as svg2png from "svg-png-converter";

@singleton()
export class CaptchaService {
  constructor(private _settings: CaptchaSettings, private _client: Client) {}
  private collector!: MessageCollector;

  getAllTextChannel = async (
    interaction: AutocompleteInteraction
  ): Promise<void> => {
    if (!interaction.isAutocomplete()) return;
    const name = interaction.options.getString("name");
    let channelToDisplay;
    if (name) {
      channelToDisplay = interaction.guild?.channels.cache
        .filter((c: GuildChannel | ThreadChannel) => {
          return compareTwoStrings(c.name, name) >= 0.3 && c.isText();
        })
        .map((c: GuildChannel | ThreadChannel) => {
          return {
            name: `${c.parent?.name ?? "Global"} > ${c.name}`,
            value: c.id,
          };
        });
    } else {
      channelToDisplay = interaction.guild?.channels.cache
        .filter((c: GuildChannel | ThreadChannel) => c.isText())
        .map((c: GuildChannel | ThreadChannel) => {
          return {
            name: `${c.parent?.name ?? "Global"} > ${c.name}`,
            value: c.id,
          };
        });
    }
    interaction.respond(channelToDisplay as ApplicationCommandOptionChoice[]);
  };

  getAllRoles = async (
    interaction: AutocompleteInteraction,
    without?: string
  ): Promise<void> => {
    if (!interaction.isAutocomplete()) return;
    const val = interaction.options.getString("name");
    let roleToDisplay;
    if (val) {
      roleToDisplay = interaction.guild?.roles.cache
        .filter((r: Role) => {
          return compareTwoStrings(r.name, val) >= 0.3;
        })
        .map((r: Role) => {
          return { name: r.name, value: r.id };
        });
    } else {
      roleToDisplay = interaction.guild?.roles.cache.map((r: Role) => {
        return { name: r.name, value: r.id };
      });
    }
    roleToDisplay = roleToDisplay?.filter((val) => {
      return (
        val.name !== "@everyone" &&
        val.name !== process.env.DISCORD_BOT_NAME &&
        val.value !== without
      );
    });
    interaction.respond(roleToDisplay as ApplicationCommandOptionChoice[]);
  };

  validGivenRole = async (
    interaction: CommandInteraction,
    key: CaptchaSettingsKey,
    not?: string
  ): Promise<void> => {
    const role = interaction.options.getString("name");
    if (role === not) {
      return interaction.reply({
        content: `You can't use <@&${role}> because it's already use at initalRole for captcha`,
        ephemeral: true,
      });
    }
    await this._settings.setSettings<CaptchaSettingsKey>(key, role);
    let reply =
      key === "givenRoleId"
        ? "as given role after captcha validation"
        : "as initial role before captcha validation";
    return interaction.reply({
      content: `You choose to use <@&${role}> ${reply}`,
      ephemeral: true,
    });
  };

  validChannel = async (interaction: CommandInteraction): Promise<void> => {
    const channel = interaction.options.getString("name");
    await this._settings.setSettings<CaptchaSettingsKey>(
      CaptchaSettingsKey.CHANNEL_ID,
      channel
    );
    return interaction.reply({
      content: `You choose too use <#${channel}> as channel for captcha`,
      ephemeral: true,
    });
  };

  toggleEnable = async (interaction: CommandInteraction) => {
    const statusStr = interaction.options.getString("status");
    if (statusStr) {
      const status: boolean = JSON.parse(statusStr);
      await this._settings.setSettings<CaptchaSettingsKey>(
        CaptchaSettingsKey.ENABLED,
        status
      );
      return interaction.reply({
        content: `Captcha antibot is now ${status ? "Enabled" : "Disabled"}`,
        ephemeral: true,
      });
    }
  };

  handleGuildMemberAdd = async (member: GuildMember) => {
    if (
      this._settings.getSettings<CaptchaSettingsKey>(CaptchaSettingsKey.ENABLED)
    ) {
      const initialRoleId = this._settings.getSettings<CaptchaSettingsKey>(
        CaptchaSettingsKey.INITIAL_ROLE_ID
      );
      if (initialRoleId) {
        member.roles.add(initialRoleId);
        const { data, text } = svgCaptcha.create({
          size: 6,
          noise: 6,
          color: true,
          fontSize: 100,
          width: 500,
          height: 200,
        });
        const imgCaptcha = await svg2png.svg2png({
          input: data,
          encoding: "buffer",
          format: "png",
        });
        this.collector = await this.sendMessageAndWaitForResult(
          member,
          imgCaptcha,
          text
        );
      }
    }
  };

  handleGuildMemberLeft = async (): Promise<void> => {
    if (this.collector) this.collector.stop();
  };

  async sendMessageAndWaitForResult(
    member: GuildMember,
    imgCaptcha: Buffer,
    text: string
  ): Promise<MessageCollector> {
    const channelId = this._settings.getSettings<CaptchaSettingsKey>(
      CaptchaSettingsKey.CHANNEL_ID
    );
    const channel = this._client.channels.cache.get(channelId) as TextChannel;
    const message = await channel.send({
      embeds: [this.createPostNewComer(member)],
      files: [
        {
          attachment: imgCaptcha,
          name: "captcha.png",
        },
      ],
    });
    const filter = (m: Message) => m.author.id === member.user.id;
    const collector = message.channel.createMessageCollector({
      filter,
    });
    collector.on("collect", async (m: Message) => {
      const content = m.content;
      if (content && content.length === 6 && content === text) {
        member.roles.remove(
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.INITIAL_ROLE_ID
          )
        );
        member.roles.add(
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.GIVEN_ROLE_ID
          )
        );
        const reply = await message.reply({
          embeds: [this.createPostValidNewComer(member)],
        });
        setTimeout(() => {
          message.delete();
          m.delete();
          reply.delete();
        }, 1000);
        member;
      } else {
        const reply = await message.reply({ content: "Bad one, try again !" });
        setTimeout(() => {
          m.delete();
          reply.delete();
        }, 1000);
      }
    });
    return collector;
  }
}
```

### Commands/captcha.ts

```typescript
import {
  ArgsOf,
  Client,
  Discord,
  On,
  Permission,
  Slash,
  SlashChoice,
  SlashGroup,
  SlashOption,
} from "discordx";
import { injectable } from "tsyringe";
import { Category } from "@discordx/utilities";
import { CaptchaService } from "../../services/security/captcha/captcha.service";
import { Logger } from "tslog";
import { AutocompleteInteraction, CommandInteraction } from "discord.js";
import { CaptchaSettings } from "../../services/security/captcha/captcha.settings";
import { CaptchaSettingsKey } from "../../services/security/captcha/captcha.keys";

const CommandName = {
  GIVEN: "given",
  INITIAL: "initial",
  ENABLE: "enable",
  CHANNEL: "channel",
};

@Discord()
@Permission(false)
@Permission({
  id: process.env.DISCORD_ADMIN_ID ?? "-1",
  type: "ROLE",
  permission: true,
})
@Category("captcha", "Commands to configure antibot captcha")
@SlashGroup("captcha", "Commands to configure antibot captcha")
@injectable()
export class Captcha {
  constructor(
    private _client: Client,
    private _service: CaptchaService,
    private _logger: Logger,
    private _settings: CaptchaSettings
  ) {}

  @Slash(CommandName.INITIAL, {
    description:
      "Role given when user join the server before captcha validation",
  })
  setInitialRole(
    @SlashOption("name", {
      description: "Role to give when user join",
      type: "STRING",
      required: true,
      autocomplete: true,
    })
    initialRoleId: string
  ) {}

  @Slash(CommandName.GIVEN as string, {
    description: "Use too choose the role for given after captcha is validated",
  })
  updateRole(
    @SlashOption("name", {
      description: "Role for captcha validation",
      type: "STRING",
      required: true,
      autocomplete: true,
    })
    roleId: string
  ) {}

  @Slash(CommandName.CHANNEL as string, {
    description: "Use too choose the channel for captcha",
  })
  updateChannel(
    @SlashOption("name", {
      description: "Channel for captcha validation",
      type: "STRING",
      required: true,
      autocomplete: true,
    })
    channelId: string
  ) {}

  @Slash(CommandName.ENABLE as string, {
    description: "Use too enable antibot by captcha verification",
  })
  enableCaptcha(
    @SlashChoice("On", "true")
    @SlashChoice("Off", "false")
    @SlashOption("status", {
      description: "On/Off",
      type: "STRING",
      required: true,
    })
    status: string
  ) {}

  @On("guildMemberAdd")
  async onGuildMemberAdd(
    [member]: ArgsOf<"guildMemberAdd">,
    client: Client,
    guardPayload: any
  ) {
    await this._service.handleGuildMemberAdd(member);
  }

  @On("guildMemberRemove")
  async onGuildMemberRemove(
    [member]: ArgsOf<"guildMemberRemove">,
    client: Client,
    guardPayload: any
  ) {
    await this._service.handleGuildMemberLeft();
  }

  @On("interactionCreate")
  async onInteractionCreate(
    [interaction]: ArgsOf<"interactionCreate">,
    client: Client,
    guardPayload: any
  ): Promise<void> {
    if ((interaction as any).commandName === "captcha") {
      if (interaction.isAutocomplete()) {
        await this.autoCompleteHandler(interaction);
      }
      if (interaction.isCommand()) {
        await this.commandHandler(interaction);
      }
    }
  }

  async autoCompleteHandler(
    interaction: AutocompleteInteraction
  ): Promise<void> {
    const command = interaction.options.getSubcommand();
    if (!Object.values(CommandName).includes(command)) {
      return;
    }
    switch (command) {
      case CommandName.CHANNEL:
        await this._service.getAllTextChannel(interaction);
        break;
      case CommandName.INITIAL:
        await this._service.getAllRoles(
          interaction,
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.GIVEN_ROLE_ID
          )
        );
        break;
      case CommandName.GIVEN:
        await this._service.getAllRoles(
          interaction,
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.INITIAL_ROLE_ID
          )
        );
        break;
    }
  }

  async commandHandler(interaction: CommandInteraction): Promise<void> {
    const command = interaction.options.getSubcommand();
    if (!Object.values(CommandName).includes(command)) {
      return;
    }
    switch (command) {
      case CommandName.INITIAL:
        await this._service.validGivenRole(
          interaction,
          CaptchaSettingsKey.INITIAL_ROLE_ID,
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.GIVEN_ROLE_ID
          )
        );
        break;
      case CommandName.GIVEN:
        await this._service.validGivenRole(
          interaction,
          CaptchaSettingsKey.GIVEN_ROLE_ID,
          this._settings.getSettings<CaptchaSettingsKey>(
            CaptchaSettingsKey.INITIAL_ROLE_ID
          )
        );
        break;
      case CommandName.CHANNEL:
        await this._service.validChannel(interaction);
        break;
      case CommandName.ENABLE:
        this._service.toggleEnable(interaction);
        break;
    }
  }
}
```

## Interface for Global Settings

```typescript
export interface IGlobalSettings {
  moduleEnabled: boolean; // module enabled yes/no
  moduleName: string; // module name
  moduleSettings: any; // module settings
  initSettings: () => Promise<Partial<Settings> | void>; // init settings if DB up
  getSettings: (key: string) => any; // get settings if DB up
  setSettings(key: string, val: any): Promise<void>; // set settings if DB up
}
```
